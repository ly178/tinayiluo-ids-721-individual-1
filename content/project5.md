+++
title = "Awards"
description = "Overview of Acheivements and Awards in Deep Learning and Statistical Modeling Competitions"
weight=5
+++

### 2024 ASA DataFest - Winner, American Statistical Association & Department of Statistical Science at Duke University
**Duration:** March 2024

![Award Picture](/icons/competition.jpg)

In this groundbreaking project, "From 'Hello World' to Beyond: An Analysis of Statistical & Data Science Learning Journey on CourseKata", our team embarked on a data-driven mission to enhance the educational impact of the CourseKata platform. The project's notable achievements include:

1. **Comprehensive Data Analysis:** We conducted an extensive analysis of a large dataset, including traditional metrics such as chapter scores and engagement levels, as well as advanced sentiment insights from student surveys. This holistic approach allowed us to gain a deep understanding of the student learning journey.

2. **Sentiment Analysis with BERT Model:** Utilizing the BERT model, a cutting-edge natural language processing technique, we performed a nuanced sentiment analysis. This innovative method provided a detailed look into the shifts in student attitudes towards coding and statistical analysis, highlighting a significant increase in satisfaction and a shift from apprehension about technical skills to an appreciation for critical thinking and active engagement.

3. **Educational Enhancements:** Leveraging our findings, we proposed and implemented strategic enhancements to the CourseKata platform. This included the introduction of coding labs tailored for non-technical majors and the application of difficulty ratings for course chapters, aimed at supporting diverse learner needs and fostering a more engaging and effective learning environment. To bolster a positive trend and enhance student satisfaction, the introduction of a chatbot could serve as an effective tool to maintain engagement and assist in the review process throughout the course, ultimately enriching the overall learning experience. 

4. **Award for Best Visualization:** Competing against 245 participants and 67 teams across only 4 award categories, we championed the "Best Visualization" Award. This accolade underscored our ability to not only analyze complex datasets but also to effectively communicate our findings through compelling and insightful visualizations.

This project exemplifies our team's proficiency in leveraging data science and machine learning to develop impactful educational solutions, significantly advancing pedagogical strategies on the CourseKata platform.

### Travelers Analytics Case Competition - Winner, Travelers Insurance
**Duration:** Oct 2023 - Jan 2024

In this competition, our team developed a sophisticated rating plan for InsNova Auto Insurance by meticulously analyzing data from 45,239 one-year vehicle insurance policies. The project's highlights include:

1. **Claim Cost Prediction Models:** We engineered three advanced models using statistical and machine learning techniques in R and Python, including Tweedie and Catboost. Our models outperformed the TRV benchmark in claim cost forecasting.
   
2. **Key Factors Identification:** Our analysis pinpointed crucial factors such as exposure, age, and vehicle value, which significantly improved the company's strategies in policy pricing, underwriting, and claims management.

3. **Competition Achievement:** Our team stood out by securing the top position against 22 teams from 7 top U.S. universities in a national data science insurance analytics competition, showcasing our analytical prowess.
