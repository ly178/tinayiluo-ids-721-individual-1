+++
title = "Education"
description = "Overview of Academic Achievements and Relevant Coursework"
weight=1
+++

I have pursued a rigorous and diverse academic path, focusing on data science, economics, and international relations. My education journey is marked by a combination of theoretical knowledge and practical skills, enriched through relevant coursework and honors.

## Duke University, The Graduate School, Durham, NC
**Period:** August 2023 - May 2025  
**Degree:** Master’s in Interdisciplinary Data Science (MIDS)  
**GPA:** 4.0  
**Relevant Coursework:** Machine Learning, Data Engineering, Natural Language Processing, Data Modeling, and Representation

At Duke University, my academic focus is on the intersection of data science disciplines. I have engaged in courses designed to provide a comprehensive foundation in machine learning, data engineering, and natural language processing, among others. My studies are directed towards harnessing data for insightful analysis and decision-making.

## New York University, College of Arts and Science, New York, NY
**Period:** August 2018 - May 2022  
**Degree:** Bachelor of Arts in Economics and International Relations (Honors)  
**GPA:** 3.85 | Magna Cum Laude  
**Distinctions:** Presidential Honors Scholar, UNGC (U.N. Global Compact) Global Campaign Representative

At New York University, I developed a strong foundation in economics and international relations, complemented by my involvement in honors programs and global initiatives. My academic achievements, underscored by a high GPA and prestigious scholarships, reflect my commitment to excellence and global engagement.

---

This educational journey has not only equipped me with a solid theoretical foundation but also honed my practical skills in data analysis, programming, and research. I am committed to leveraging this background to contribute to the field of data science, where I continuously seek to apply my skills in real-world scenarios and research endeavors.

Feel free to connect with me for discussions on data science, machine learning, and how interdisciplinary approaches can address complex challenges in today's data-driven world.
