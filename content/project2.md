+++
title = "Skills"
description = "Overview of Statistical Modeling and Programming Skills"
weight=2
+++

My technical proficiency spans a broad range of programming languages, data analysis tools, statistical methods, and various frameworks and platforms. Here's an outline of my core skills and competencies:

**Programming Languages**:
- Python: Experienced with libraries such as NumPy, Pandas, Polars, Matplotlib, Seaborn, Scikit-learn, PyTorch, and NLTK for data manipulation, visualization, and machine learning.
- Rust: Utilized for system-level programming and developing efficient, safe software.

**Data Analysis Tools**:
- RStudio: Proficient with packages like dbplyr, ggplot2, tidyverse, glm, car, caret, ExpData, and DataExplorer for data cleaning, visualization, and analysis.
- SQL & Excel: Skilled in database management and spreadsheet analysis for extracting and analyzing data.

**Statistical Methods**:
- Well-versed in Regression, Classification, Decision Trees, Hypothesis Testing, Cluster Analysis, Time Series Analysis, and Neural Networks for predictive modeling and data analysis.

**Tools & Frameworks**:
- Data Visualization: Expertise in Tableau and Power BI for creating impactful, insightful visualizations.
- Development & Operations: Proficient with Git, Spark, Docker, Linux, and Emacs for version control, data processing, containerization, operating systems, and text editing.
- Cloud Platforms: Skilled in deploying and managing resources on AWS, Azure, GCP, and leveraging Github CodeSpaces for development environments.

This skill set has been instrumental in my pursuit of data science, enabling me to tackle complex data challenges and contribute meaningful insights across various projects.
