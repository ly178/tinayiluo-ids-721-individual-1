+++
title = "Professional Experience"
description = "Overview of My Professional Journey in the Legal and Tech Industry"
weight=6
+++

### Data Scientist Intern at LexisNexis, Raleigh, NC
**Duration:** June 2024 - Sep 2024

During my internship, I focused on enhancing model performance, ensuring system reliability, and collaborating with stakeholders to integrate data science solutions. Achievements include:

1. **Model Performance and Accuracy:** Enhanced model performance and accuracy by 30% and ensured 0% hallucination by leading the development of Lexis+AI across the UK, Canada, and Europe. Implemented LLMs with RAG (Retrieval-Augmented Generation) functionality on 3.8 PB of global content, engaging in extensive prompt engineering and search engine optimization to enhance platform reach and functionality.

2. **System Reliability:** Ensured robust and reliable system performance by researching, building, and deploying models trained on 138 billion documents and records, with 2.2 million new legal documents added daily. Addressed complex problems in NLP (natural language processing), machine learning, and information retrieval, while managing debugging and functionality-building within the mono repository using Azure DevOps.

3. **Stakeholder Collaboration:** Conducted model reviews with key stakeholders, including those with limited statistical backgrounds, effectively integrating data science solutions and fostering collaborative decision-making.

### Business and Legal Intern at Tsang & Associates, PLC, Los Angeles, CA
**Duration:** July 2022 - May 2023

In this role, I employed data analytics to significantly enhance case approval rates and represent corporate clients effectively. Key contributions include:

1. **Financial Modeling and Analysis:** Increased EB-2 and EB1-A case approval rates by 20% from the previous year by identifying and analyzing omitted net current assets and intangible assets from tax returns. Utilized Tableau for advanced financial modeling visualization, enabling clearer decision-making and strategy formulation.

2. **Corporate Client Representation:** Successfully represented over 20 international business cases to USCIS and other government entities. Crafted comprehensive financial presentations that showcased clients' financial strength and promising economic prospects, incorporating relevant case laws and data-driven insights, which effectively communicated the value and legitimacy of our clients' cases.

3. **Revenue Growth through Strategic Case Management:** Contributed to over 30% of the regional company's revenue by developing innovative case strategies through extensive data analysis and timely research. Maintained direct communication with government agencies such as DOS, DHS, DOL, and IRS, ensuring the efficient progression of cases and client satisfaction.

### Marketing Analyst Intern at Technical Consulting & Research, Inc. (TCR, Inc.), New York, NY
**Duration:** June 2020 - Aug 2020

During my internship, I focused on enhancing marketing efforts and campaign efficiency through statistical analysis and market research. Achievements include:

1. **Marketing Optimization:** Enhanced website conversion rates by 10% and improved email marketing conversion rates by 5% through in-depth statistical analysis and experimental designs, including A/B testing. Utilized CRM System, Google Analytics, R, and Python for data analysis and insights derivation.

2. **Campaign Efficiency and Customer Engagement:** Raised campaign efficiency by 15% and elevated customer engagement by 10% through comprehensive marketing research. Employed Power BI and the Alexa Website Ranking System for market data gathering and benchmarking, enabling more targeted and effective marketing strategies.

3. **Cybersecurity Research Reports:** Conducted two research reports and presentations on cybersecurity training programs tailored to companies serving as Department of Defense (DOD) contractors. This work involved analyzing the cybersecurity frameworks CMMC and NIST, providing valuable insights for companies to enhance their security measures and compliance.
