+++
title = "Projects"
description = "Overview of Software Development, Statistical Modeling, and Machine Learning Projects"
weight=4
+++

### Movie Recommendation Web App, Duke University
**Duration:** Mar 2024 - May 2024

Developed a Rust-based web service to deploy open-source models for a movie recommendation system. Key achievements include:

1. **Model Integration:** Utilized Rust-BERT for generating embeddings of IMDb movie descriptions stored in Qdrant, OpenLLaMA for generating descriptions from user keywords, and Cohere for creating embeddings from user inputs.

2. **User Interface:** Designed a user-friendly frontend with HTML and JavaScript, enabling users to input movie descriptions or keywords with autofill, and receive enriched recommendations including descriptions, release years, directors, and ratings.

3. **Deployment:** Containerized the service for deployment on AWS ECS and Fargate, implementing a CI/CD pipeline to streamline build, test, and deployment processes, ensuring seamless and efficient deployment. Enhanced system robustness by incorporating monitoring, metrics collection, and comprehensive documentation.

### Impact of Image Preprocessing on Diabetic Retinopathy Detection, Duke University
**Duration:** Mar 2024 - May 2024

This project aimed to improve diabetic retinopathy (DR) detection models through advanced preprocessing techniques. Key accomplishments include:

1. **Model Enhancement:** Implemented EfficientNetB0 with preprocessing techniques (cropping, luminosity adjustment, and color normalization) applied post-image resizing and pre-data augmentation.

2. **Performance Improvement:** Achieved substantial accuracy improvements, enhancing performance to 70.8% on Messidor (a 12.5% increase) through preprocessing combinations of cropping and color normalization.

3. **Healthcare Impact:** Developed robust DR detection systems with the potential for significant impact in early screening and intervention, improving outcomes for diabetic retinopathy patients worldwide.

### Insurance Coverage and Lung Cancer Diagnosis in the United States of America, Duke University
**Duration:** Mar 2024 - May 2024

Revealed a positive impact of insurance coverage on lung cancer diagnosis rates in the U.S. Key accomplishments include:

1. **Causal Inference Analysis:** Conducted a causal inference analysis using matching techniques and linear probability regression with data from the National Health Interview Survey (NHIS) spanning 2000 to 2018.

2. **Policy Impact:** Demonstrated that transitioning from no insurance to coverage increased lung cancer diagnosis rates, highlighting the Affordable Care Act's (ACA) positive impact.

3. **Policy Recommendations:** Identified cost and unemployment as primary barriers to insurance coverage, suggesting policy changes to expand coverage could improve early diagnosis and healthcare outcomes.

### YouTube Video Classifier, Duke University
**Duration:** Oct 2023 - Dec 2023

This project aimed at boosting targeted advertising efficiency by automating the categorization of YouTube videos into music or sports categories. We achieved remarkable success with:

1. **Classification Accuracy:** Utilizing over 60,000 video tag records, we applied classification algorithms and attained more than 96% testing accuracy using both raw count and TF-IDF embedding techniques in the Naive Bayes model, and over 89.57% testing accuracy with a Recurrent Neural Network (RNN) model.

### Nutrition Guide Web App, Duke University
**Duration:** Oct 2023 - Dec 2023

Developed "Nutrition Guide," a dynamic web application microservice, this project interfaced seamlessly with the Databricks data pipeline. Key accomplishments include:

1. **Technology Integration:** Utilized Docker for containerization, along with Azure App Services and Flask for efficient deployment and management.
   
2. **Data Processing Optimization:** Enhanced Databricks ETL-Query pipelines with PySpark and Databricks-SQL, and utilized Pandas, Matplotlib, and Seaborn for in-depth data visualization and analysis.
   
3. **Performance and Workflow Improvements:** Conducted load testing to ensure stability under heavy loads and streamlined CI/CD processes with GitHub Actions, significantly reducing deployment errors and accelerating deployment times.

### Stack Overflow Developer Salary Prediction & Satisfaction Analysis, Duke University
**Duration:** Oct 2023 - Dec 2023

This project focused on the deployment of R models to predict compensation and analyze job satisfaction among software engineers, using a dataset of responses from 89,184 participants across 185 countries in 2023. Key methodologies included multilinear and ordinal regression techniques. The study revealed significant findings:

1. **Impact of Organizational Size:** It was found that engineers at smaller companies (2-9 employees) are 2.35 times more likely to report job satisfaction than those in larger organizations, underscoring the influence of company size on employee well-being.
   
2. **Salary Influencing Factors:** The research provided detailed insights into the various elements affecting tech sector salaries, offering valuable guidance for stakeholders in making educated compensation decisions.

### Opioids Control Policy Analysis, Duke University
**Duration:** Oct 2023 - Dec 2023

This project critically evaluated the efficacy of opioid management strategies in various states, concentrating on the implementation within Florida. The comparative analysis included Texas and Washington, utilizing Pre-Post and Difference-in-Differences methodologies, complemented by the analysis of socioeconomic indicators to select control states, encompassing:

1. **Data Processing:** Over 6GB of data from mortality records, DEA pill shipments, and NHGIS population figures for 2006-2015 were meticulously cleaned, merged, and analyzed using Python, providing a comprehensive overview of opioid-related trends.
   
2. **Policy Effectiveness:** The findings underscored the success of Florida's comprehensive approach to opioid regulation and highlighted the critical role of policy in managing public health crises.

### LLM Text Generator, Duke University
**Duration:** Aug 2023 - Sep 2023

This project entailed the development of a versatile natural language text generator, leading to significant advancements in automated content creation. Highlights include:

1. **Model Accuracy and Innovation:** By training on datasets exceeding 10k entries, we achieved over 93% accuracy in next-word prediction using Bare-Bones Markov and POS Hidden Markov models, alongside sophisticated techniques for enhanced performance.

### Airbnb Price Generator, Duke University
**Duration:** Aug 2023 - Sep 2023

The Airbnb Price Generator project involved creating an R Model to accurately forecast future pricing for hosts. Key achievements were:

1. **Model Precision:** Developed a multilinear regression model, trained on an extensive dataset of 3,239 observations and 75 variables, which included critical market influencers like user reviews and facility standards, resulting in a RMSE lower than 0.4.
   
2. **Efficiency and Accuracy Enhancements:** Improved the model's performance by over 30% through the application of advanced data science tools, including tidyverse, caret, metrics, and DataExplorer.

### UNGC (U.N. Global Compact) Global Campaign, New York University
**Duration:** Feb 2021 - Apr 2021

Conducted data collection, analysis, and research for responsible global ESG investing. Key accomplishments include:

1. **Financial Strategies:** Created data-driven financial strategies that assess non-financial factors, including greenhouse gas emissions, supply chain transparency, and product sustainability, to inform investment decisions.
2. **Presentation:** As the team representative, presented the project "Responsible Corporate Practices for Extractives" to UNGC.