+++
title = "Research"
description = "Overview of Research in Statistical Analysis and Policy Evaluation"
weight=3
+++

### DURF (Deans Undergraduate Research Fund), New York University
**Duration:** Sep 2021 - May 2022

Under the auspices of the DURF, this project explored the impact of e-commerce on rural poverty reduction in China, specifically through the lens of Ali Taobao E-Commerce Villages. This involved:

1. **Statistical Analysis:** Using Python and R, a two-way fixed effect panel data regression analysis was conducted on data from 2,591 counties across 27 provinces, spanning from 2014 to 2021.
   
2. **Global Poverty Alleviation:** The research contributed valuable insights into e-commerce strategies for rural development, marking a significant step towards understanding the role of digital platforms in poverty reduction efforts. The study was featured in the 2022 edition of NYU Inquiry: A Journal of Undergraduate Research.
