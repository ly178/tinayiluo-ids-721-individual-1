+++
title = "About"
path = "about"
[extra]
cover_image="icons/test2.JPG"
+++

Hi there! I'm Tina, a data enthusiast with a passion for exploring and analyzing datasets to uncover insights and tell data-driven stories. 

I'm currently pursuing a Master's in Interdisciplinary Data Science at Duke University, building on my solid foundation in Economics and International Relations, where I graduated Magna Cum Laude and earned the title of Presidential Honors Scholar at New York University.

My journey in the world of data analysis involves a diverse skill set, including programming in Python and Rust, data analysis with tools like RStudio, SQL, and Excel, and proficiency in statistical methods such as Time Series, Decision Trees, Regression Models, and Cluster Analysis. I'm also well-versed in data visualization tools like Tableau and Power BI, and I navigate the world of Git, Spark, and cloud platforms including AWS and Azure with ease.

I'm constantly seeking opportunities to expand my horizons, and my recent projects at Duke University, including Natural Language Processing for text generation and Airbnb price forecasting, reflect my commitment to staying at the forefront of data science. Let's connect and explore the exciting realms of machine learning and natural language processing. Feel free to reach out for data-driven discussions and collaborations!

917-543-8862 | tina.yi@duke.edu | [LinkedIn](https://www.linkedin.com/in/luopeiwen-yi/) | [GitHub](https://github.com/tinayiluo0322) | [Youtube](https://www.youtube.com/channel/UC8GCh0scMLtrtV6qyYw7arQ)
